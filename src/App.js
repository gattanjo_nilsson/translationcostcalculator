import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import TranslationCostCalculator from './components/TranslationCostCalculator/TranslationCostCalculator';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Translation Cost Calculator</h2>
        </div>
        <TranslationCostCalculator/>
      </div>
    );
  }
}

export default App;
