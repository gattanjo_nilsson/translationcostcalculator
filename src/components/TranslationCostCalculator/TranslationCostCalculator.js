import React from 'react';
import './TranslationCostCalculator.css';

export default class TranslationCostCalculator extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
      price: 0,
      selectedOption: '',
      totalPrice: 0,
    };

    this.handleTextChange = this.handleTextChange.bind(this);
    this.handlePriceChange = this.handlePriceChange.bind(this);
    this.handleOptionChange = this.handleOptionChange.bind(this);
    this.calculate = this.calculate.bind(this);
  }

  handleTextChange(event) {
    this.setState({text: event.target.value});
  }

  handleOptionChange(event) {
    this.setState({ selectedOption: event.target.value });
  }

  handlePriceChange(event) {
    this.setState({ price: event.target.value });
  }

  countWords(text) {
    return text.split(" ").length;
  }

  countLetters(text) {
    return text.length;
  }

  calculate() {
    let text = this.state.text.trim();
    let price = parseFloat(this.state.price);
    let totalPrice;

    switch (this.state.selectedOption) {
      case 'WORD':
        totalPrice = this.countWords(text) * price;
        break;

      case 'LETTER':
        totalPrice = this.countLetters(text) * price;
        break;

      default:

    }
    this.setState({totalPrice: totalPrice});
  }

  render() {

    return(
      <div className="container">
        <textarea className="text-area" value={this.state.text} onChange={this.handleTextChange} />
        <br></br>
        <label>
          Price:
          <input type="text" value={this.state.price} onChange={this.handlePriceChange} />
        </label>

        <form>
          <div className="radio">
            <label>
              <input type="radio" value="WORD"
                checked={this.state.selectedOption === 'WORD'}
                onChange={this.handleOptionChange} />
              Price per word
            </label>
          </div>
          <div className="radio">
            <label>
              <input type="radio" value="LETTER"
                checked={this.state.selectedOption === 'LETTER'}
                onChange={this.handleOptionChange} />
              Price per letter
            </label>
          </div>
        </form>
        <button type="button" onClick={this.calculate}>Calculate</button>
        <p>{this.state.totalPrice}</p>
      </div>
    );
  }
}
